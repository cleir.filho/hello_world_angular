import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  nome : string = "cleir de castro e costa filho"
  @Input() compar: string = "";

  constructor() { }

  ngOnInit(): void {
  }

}
